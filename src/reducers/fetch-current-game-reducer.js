import {actions} from "../actions/actions";

const initialState = {
    players: [],
    role: null,
    gameStatus: 'ACTIVE',
    totalPlayers: -1
};

export const currentGame = (state = initialState, action) => {
    switch (action.type) {
        case actions.CURRENT_GAME_FETCHED_SUCCESS_ACTION: {
            return {
                ...state,
                players: action.payload.players,
                role: action.payload.players != null ? action.payload.players.filter(p => p.name === action.userId)[0].role : null,
                gameStatus: action.payload.gameStatus,
                totalPlayers: action.payload.totalPlayers
            };
        }
        case actions.TEAM_INFO_REVEALED_ACTION: return {
            ...state,
            players: action.payload.players,
        };
        case actions.PLAYERS_JOINED_ACTION:
            return action.player == null ? {...state} :
                {...state,
                players: [...state.players, {
                    name: action.player}]};
        case actions.START_GAME_SUCCESS_ACTION:
            return {...state,
                gameStatus: 'STARTED',
                role: action.payload.role,
            };

        case actions.ONLINE_OFFLINE_ACTION: return {
            ...state,
            players: state.players.map(p => {
                if(p.name === action.payload.sender)
                    p.offline = action.payload.notifyFlag;
                return p;
            })
        };
        default: return state;
    }
};
