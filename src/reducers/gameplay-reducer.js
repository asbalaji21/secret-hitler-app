import {actions} from "../actions/actions";

const initialState = {
    electionTracker: 0,
    liberalPolicy: 0,
    fascistPolicy: 0,
    roundCompleted: false,
    president: null,
};

export const gamePlay = (state = initialState, action) => {
    switch (action.type) {
        case actions.VOTE_FAILED_ACTION: return {
            ...state,
            electionTracker: action.payload.electionTracker,
            roundCompleted: true
        };
        case actions.POLICY_ENACTED_ACTION: return {
            ...state,
            liberalPolicy: action.payload.liberal ? state.liberalPolicy + 1: state.liberalPolicy,
            fascistPolicy: action.payload.liberal ? state.fascistPolicy : state.fascistPolicy + 1,
            electionTracker: 0,
        };
        case actions.GAME_PLAY_FETCH_ACTION: return {
            ...state,
            fascistPolicy: action.payload.policy == null ? 0 : action.payload.policy.enactedFascistPolicies,
            liberalPolicy: action.payload.policy == null ? 0 : action.payload.policy.enactedLiberalPolicies,
            electionTracker: action.payload.electionTracker,
            roundCompleted: action.payload.roundCompleted,
            president: action.payload.president,
        };
        case actions.ELECT_PRESIDENT: return {
            ...state,
            president: action.payload.president,
            roundCompleted: false,
        };
        case actions.SPL_POWER_USED_ACTION: return {
            ...state,
            roundCompleted: true,
        };
        case actions.ELECT_PRESIDENT_CLICK_ACTION: return {
            ...state,
            roundCompleted: false,
        };
        default: return state;
    }
};
