import {combineReducers} from "redux";
import { chatInputReducer } from "./chat-input-reducer";
import {fetchActiveGames} from "./fetch-active-games-reducer";
import {createJoinGame} from "./create-game-reducer";
import {currentGame} from "./fetch-current-game-reducer";
import {messageReducer} from "./message-reducer";
import {gamePlay} from './gameplay-reducer';
import { noVotersReducer } from "./no-voters-reducer";

export const rootReducer = combineReducers({
    chatInput: chatInputReducer,
    fetchActiveGames: fetchActiveGames,
    createJoinGame: createJoinGame,
    currentGame: currentGame,
    messageReducer: messageReducer,
    gamePlay: gamePlay,
    noVotersReducer: noVotersReducer,
});
