import {actions} from "../actions/actions";

const initialState = [];

export const messageReducer = (state=initialState, action) => {
    switch (action.type) {
        case actions.ALL_MESSAGES_RECEIVED_ACTION: return action.payload;
        case actions.CURRENT_MESSAGE_RECEIVED_ACTION: return [
            ...state, action.payload ];
        case actions.UPDATE_MESSAGE:
            return [...state].map(msg => msg.messageId === action.payload.messageId ? action.payload : msg);
        default: return state;
    }
};
