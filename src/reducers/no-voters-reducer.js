import {actions} from "../actions/actions";

const initialState = {
    shouldShowToast: false,
    message: null,
};

export const noVotersReducer = (state=initialState, action) => {
    if(action.type === actions.NO_VOTERS_ACTION)
        return { message: action.payload.players.length === 0 ? 'Everyone voted YES' : `${action.payload.players.map(p => p.name).join(', ')} voted NO`, shouldShowToast: true};
    if(action.type === actions.TOAST_HIDE_ACTION)
        return {message: null, shouldShowToast: false};
    return state;
};
