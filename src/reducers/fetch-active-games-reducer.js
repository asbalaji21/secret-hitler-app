import {actions} from "../actions/actions";

const initialState = [];

export const fetchActiveGames = (state = initialState, action) => {
    switch (action.type) {
        case actions.ACTIVE_GAMES_FETCHED_SUCCESS_ACTION: {
            state = action.payload;
            return state;
        }
        default: return state;
    }
};
