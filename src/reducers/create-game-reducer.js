import {actions} from "../actions/actions";

const initialState = {
    isDialogOpen: false,
    gameId: 0,
};

export const createJoinGame = (state=initialState, action) => {
    switch (action.type) {
        case actions.GAME_DIALOG_TOGGLE_ACTION: return {
            ...state,
            isDialogOpen: !state.isDialogOpen
        };
        case actions.GAME_JOINED_ACTION: return {
                ...state,
                gameId: action.gameId,
            };
        default: return state;
    }
};
