import { actions } from "../actions/actions";

const initialState = {
    message: '',
    isEmojiOpen: false,
    shouldAutoScroll: true,
    newMessageCount: 0,
    typing: null,
};

export const chatInputReducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.EMOJI_TOGGLE_ACTION: return {
            ...state,
            isEmojiOpen: !state.isEmojiOpen
        };
        case actions.EMOJI_CLICK_ACTION: return {
            ...state,
            message: state.message + action.payload
        };
        case actions.MESSAGE_SEND_ACTION:
            return {
            ...state,
            message: '',
        };
        case actions.MESSAGE_CHANGE_ACTION: return {
            ...state,
            message: action.payload
        };
        case actions.TOGGLE_AUTO_SCROLL: return {
            ...state,
            shouldAutoScroll: action.shouldAutoScroll,
            newMessageCount: action.shouldAutoScroll ? 0 : state.newMessageCount,
        };
        case actions.CURRENT_MESSAGE_RECEIVED_ACTION: return {
            ...state,
            newMessageCount: state.shouldAutoScroll ? 0 : state.newMessageCount + 1,
        };
        case actions.UPDATE_TYPING_ACTION: return {
            ...state,
            typing: action.payload.typing && action.payload.sender !== sessionStorage.getItem('userId') ? action.payload.sender: null,
        };

        default: return state;
    }
};
