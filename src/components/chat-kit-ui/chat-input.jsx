import React, {useRef} from 'react';
import {TextField, makeStyles, Box, InputAdornment, IconButton} from '@material-ui/core';
import { InsertEmoticon, Send } from '@material-ui/icons';
import {useDispatch, useSelector} from "react-redux";
import { actions } from "../../actions/actions";

const useStyles = makeStyles({
    textField: {
        width: '100%',
    }
});

export const ChatInput = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    let message = useSelector(state => state.chatInput.message);
    const gameId = sessionStorage.getItem('gameId');
    const userId = sessionStorage.getItem('userId');
    const textFieldRef = useRef(null);
    const onKeyPress = event => {
        if (event.charCode === 13) { // enter key pressed
            event.preventDefault();
            if(message === '')
                return;
            dispatch({type: actions.TOGGLE_AUTO_SCROLL, shouldAutoScroll: true});
            dispatch({type: actions.MESSAGE_SEND_ACTION, payload: {message: message,
                    gameId: gameId,
                    sender: userId}});
            textFieldRef.current.focus();
            textFieldRef.current.click();
        }
    };

    return <Box width={1} style={{ position: 'sticky', bottom: 0 }}>
            <TextField inputRef={textFieldRef} variant='outlined' margin='dense' onKeyPress={onKeyPress} className={classes.textField} label='' placeholder="Enter your message" value={message}
                       onChange={event => dispatch({type: actions.MESSAGE_CHANGE_ACTION, payload: event.target.value})}
                       onFocus={() => dispatch({type: actions.TYPING_ACTION, payload: {gameId: gameId, sender: userId, isNotifyFlag: true}})}
                       onBlur={() => dispatch({type: actions.TYPING_ACTION, payload: {gameId: gameId, sender: userId, isNotifyFlag: false}})}
            InputProps={{
                style: {
                    paddingRight: '0px',
                    paddingLeft: '0px',
                    backgroundColor: 'white',
                },
                startAdornment: (<InputAdornment position='start'><IconButton onClick={() => dispatch({type: actions.EMOJI_TOGGLE_ACTION})}><InsertEmoticon aria-label="grin-face"/>
                </IconButton></InputAdornment>),
                endAdornment: (<InputAdornment position='end'><IconButton onClick={() => dispatch({type: actions.MESSAGE_SEND_ACTION, payload: {
                        message: message,
                        gameId: gameId,
                        sender: userId
                    }})}><Send aria-label='send'/></IconButton></InputAdornment>)
            }}
            />
        </Box>
};
