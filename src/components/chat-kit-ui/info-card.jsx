import React from 'react';
import {
    Card,
    CardContent,
    List,
    ListItemText, Grid, Divider, CardMedia
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import classNames from "clsx";
import {GetRoleColorClass, getRoleImage} from "../../role-fetcher";

const useStyles = makeStyles(theme => ({
    card: {
        borderRadius: 15,
        minWidth: 175,
        minHeight: 200,
        margin: theme.spacing(1.5),
    },
    media: {
        height: 100,
        backgroundSize: '50%',
        marginTop: theme.spacing(-1.5),
    },
}));

export const InfoCard = props =>
{
    const classes = useStyles();
    return <Grid container justify='center'>
        <Grid item>
        <Card className={classNames(classes.card, GetRoleColorClass(props.label))}>
        <CardContent>
            <List>
                <ListItemText key={props.heading} style={{ fontWeight: 'bold', textAlign: 'center' }}>
                    {props.heading}
                </ListItemText>
                <Divider/>
            </List>
        </CardContent>
            <CardMedia image={getRoleImage(props.label)} className={classes.media}/>
    </Card>
        </Grid>
    </Grid>;
};
InfoCard.propTypes = {
    heading: PropTypes.string,
    label: PropTypes.string,
};
