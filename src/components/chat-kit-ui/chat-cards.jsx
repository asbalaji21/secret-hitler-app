import React from 'react';
import { ChatCard } from './chat-card';
import {Grid} from '@material-ui/core';
import PropTypes from 'prop-types';
import {getRoleImage, GetRoleColorClass} from "../../role-fetcher";
import {ChatEvent} from "./chat-event";

export const ChatCards = ({ chatCards, onCardClick, messageId, heading }) => {
    const GetColor = (role, selected) => {
        return selected != null && selected.includes(sessionStorage.getItem('userId')) ? GetRoleColorClass('NEUTRAL') :
            GetRoleColorClass(role);
    };
       return <ChatEvent heading={heading}><Grid container justify='center'>
        {chatCards.map(chatCard => <Grid item key={chatCard.chipCardId}>
            <ChatCard role={chatCard.label}
                      image={getRoleImage(chatCard.label)}
                      color={GetColor(chatCard.label, chatCard.selected)}
                      onCardClick={() => onCardClick({...chatCard, messageId: messageId})}
                      disabled={chatCard.disabled}
                      selected={chatCard.selected}
            />
        </Grid>)}
       </Grid></ChatEvent>};

ChatCards.propTypes = {
    chatCards: PropTypes.array,
    onCardClick: PropTypes.func,
    heading: PropTypes.string,
};
