import React from 'react';
import { Grid, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import classNames from 'clsx';
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
    root: { flexGrow: 1 },
    paper: {
        height: 'auto',
        width: 'auto',
        minHeight: 20,
        minWidth: 240,
        maxWidth: 260,
        padding: theme.spacing(1),
        margin: theme.spacing(1.5),
        backgroundColor: '#ffd7b5',
        borderRadius: '0 15px 15px 15px',
    },
    control: {
        padding: theme.spacing(1),
    },
    senderLabel: {
        fontSize: '0.65rem',
        fontWeight: 'bold',
    },
    time: {
        fontSize: '0.65rem',
        fontWeight: 'bold',
        color: '#555',
        paddingTop: theme.spacing(0.25),
    },
    message: {
        fontSize: '0.85rem',
        paddingTop: theme.spacing(0.25),
    },
    sender: {
        backgroundColor: 'rgba(225,255,198,0.64)',
        borderRadius: '15px 0 15px 15px',
    },
}));

export const ChatBubble = props =>
{
    const classes = useStyles();


    return <Grid container direction={props.right ? 'row-reverse' : 'row'}>
        <Grid item>
            <Paper className={classNames(classes.paper, props.right && classes.sender, props.className)} elevation={6}>
                {!props.right && <Typography className={classes.senderLabel}>
                    {props.senderLabel}
                </Typography>}
                <Typography  className={classes.message}>
                    {props.message}
                </Typography>
                <Typography className={classes.time} align='right'>
                    {props.time}
                </Typography>
            </Paper>
        </Grid>
    </Grid>;
};

ChatBubble.propTypes = {
    right: PropTypes.bool,
    senderLabel: PropTypes.string,
    message: PropTypes.string,
    time: PropTypes.string,
    className: PropTypes.string,
};
