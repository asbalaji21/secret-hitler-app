import React from 'react';
import {AppBar, Typography, IconButton, Toolbar, Grid, Avatar, Box} from '@material-ui/core';
import {ArrowBack, Menu, DoubleArrow} from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {getRoleColor, getRoleImage} from "../../role-fetcher";
import {useHistory} from "react-router";
import SideDrawer from "../side-drawer";
import {useSelector} from "react-redux";


const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(-2),
    },
    app: {
        backgroundColor: theme.palette.secondary.light
    },
    header: {
        marginBottom: theme.spacing(-1),
        paddingBottom: theme.spacing(-2),
    },
    subTitle: {
        marginTop: theme.spacing(0),
        paddingTop: theme.spacing(0),
        color: '#ddd',
    },
    avatar: {
        margin: 10,
        marginRight: theme.spacing(2),
        marginLeft: theme.spacing(0),
        backgroundSize: '30%',
        backgroundColor: 'grey'
    },
    toolbar: {
        cursor: 'pointer',
    },
    button: {
        color: 'white'
    }
}));

const ChatHeader = props => {

    const history = useHistory();
    const roundCompleted = useSelector(state => state.gamePlay.roundCompleted);
    const president = useSelector(state => state.gamePlay.president);
    const isRoundCompleted = roundCompleted && sessionStorage.getItem('userId') === president;
    const classes = useStyles();
    return <Box width={1} style={{ position: 'sticky', top: 0}}>
            <AppBar position='sticky'>
            <Toolbar className={classes.toolbar}>
                <IconButton edge="start" color='inherit' aria-label='arrow-back' onClick={()=>history.goBack()}>
                    <ArrowBack />
                </IconButton>
                <Avatar alt='Header Logo' src={getRoleImage(props.role)}
                        style={{backgroundColor: getRoleColor(props.role)}}
                        className={classes.avatar}/>
                <Grid container direction='column' className={classes.title} justify='flex-end'>
                    <Grid item>
                        <Typography variant='h6' className={classes.header}>
                            {props.title}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant='caption' className={classes.subTitle}>
                            {props.subTitle}
                        </Typography>
                    </Grid>
                </Grid>
                {isRoundCompleted && <IconButton edge="start" color='inherit' aria-label='menu' onClick={props.electPresident}>
                    <DoubleArrow/>
                </IconButton>}
                {!isRoundCompleted && <IconButton edge="start" color='inherit' aria-label='menu' onClick={props.toggleDrawer}>
                    <Menu/>
                </IconButton>}
            </Toolbar>
        </AppBar>
        <SideDrawer open={props.drawerOpen} toggleDrawer={props.toggleDrawer} drawerChildren={props.drawerChildren}/>
    </Box>
};

ChatHeader.propTypes =
    {
        title: PropTypes.string,
        subTitle: PropTypes.string,
        role: PropTypes.string,
        drawerChildren: PropTypes.object,
        drawerOpen: PropTypes.bool,
        toggleDrawer: PropTypes.func,
        electPresident: PropTypes.func,
    };

export default ChatHeader;
