import React from 'react';
import { Grid, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import classNames from 'clsx';
import { GetRoleColorClass } from "../../role-fetcher";

const useStyles = makeStyles(theme => ({
    root: { flexGrow: 1 },
    paper: {
        height: 'auto',
        width: 'auto',
        maxWidth: 400,
        padding: theme.spacing(1),
        margin: theme.spacing(1.5),
        borderRadius: '10px',
    },
    control: {
        padding: theme.spacing(1),
    },
    info: {
        fontSize: '0.85rem',
        overflow: 'auto',
        whiteSpace: 'pre',
        paddingTop: theme.spacing(0.25),
    },
}));

export const ChatInfo = (props) => {
    const classes = useStyles();

    return <Grid container justify='center'>
        <Grid item>
            <Paper className={classNames(classes.paper, props.className, GetRoleColorClass(props.colorType))} elevation={2}>
                <Typography className={classes.info}>
                    {props.message}
                </Typography>
            </Paper>
        </Grid>
    </Grid>;
};
ChatInfo.propTypes = {
    message: PropTypes.string,
    colorType: PropTypes.string,

};
