import React from 'react';
import {CardMedia, Card} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import classNames from 'clsx';
import PropTypes from 'prop-types';
import {grey} from "@material-ui/core/colors";

const useStyles = makeStyles(theme => ({
    card: {
        padding: theme.spacing(1),
        margin: theme.spacing(1.5),
        borderRadius: 15,
        cursor: 'pointer',
        width: 60,
        height: 60
    },
    cardSelected: {
        //border: '6px solid rgba(0,0,0,0.5)',
        backgroundColor: grey,

    },
    media: {
        height: 60,
        backgroundSize: '50%',
    },
    content: {
        color: '#222',
    },
    disabled: {
      opacity: '0.7',
    }
}));

export const ChatCard = props =>
{
    const classes = useStyles();
    const isCardSelected = props.selected != null && props.selected.includes(sessionStorage.getItem('userId'));
    return <Card className={classNames(classes.card, props.color, isCardSelected && classes.cardSelected, props.disabled && classes.disabled)}>
        <CardMedia onClick={props.onCardClick} image={props.image} className={classNames(classes.media, props.className, props.color)}/>
    </Card>;
};
ChatCard.propTypes = {
    color: PropTypes.string,
    image: PropTypes.string,
    onCardClick: PropTypes.func,
    role: PropTypes.string,
    disabled: PropTypes.bool,
    selected: PropTypes.array,
};
