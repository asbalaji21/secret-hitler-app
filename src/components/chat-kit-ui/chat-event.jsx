import React from 'react';
import {
    Card,
    CardContent,
    List,
    ListItemText, Grid, Divider
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
    card: {
        borderRadius: 15,
        minWidth: 250,
        backgroundColor: '#efefef',
        margin: theme.spacing(1.5),
    },
}));

export const ChatEvent = props =>
{
    const classes = useStyles();
    return <Grid container justify='center'>
        <Grid item>
        <Card className={classes.card}>
        <CardContent>
            <List>
                <ListItemText key={props.heading} style={{ fontWeight: 'bold' }}>
                    {props.heading}
                </ListItemText>
                <Divider/>
                    { props.children }
            </List>
        </CardContent>
    </Card>
        </Grid>
    </Grid>;
};
ChatEvent.propTypes = {
    heading: PropTypes.string,
};
