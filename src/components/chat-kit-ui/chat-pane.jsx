import React, {useRef, useEffect, useState} from 'react';
import { ChatBubble } from './chat-bubble';
import { ChatInfo } from './chat-info';
import { ChatCards } from './chat-cards';
import { ChatList } from "./chat-list";
import PropTypes from 'prop-types';
import { ChatChips } from './chat-chips';
import {InfoCard} from "./info-card";
import {useDispatch, useSelector} from "react-redux";
import {ArrowDownward} from "@material-ui/icons";
import {makeStyles, IconButton, Badge} from "@material-ui/core";
import {NoVoterToast} from "../no-voter-toast";
import {useBottomScrollListener} from "react-bottom-scroll-listener";
import {UnReadDivider} from './unread-divider';
import {actions} from "../../actions/actions";

const useStyles = makeStyles({
    downArrow: {
        margin: 0,
        top: 'auto',
        right: 20,
        bottom: 70,
        left: 'auto',
        position: 'fixed'
    },
    downArrowIcon: {
        backgroundColor: '#eee',
    },
    chatPane: {
        height: window.innerHeight - 112,
        overflow: 'auto'
    }
});

export const ChatPane = props =>
{
    const classes = useStyles();
    const chatEntries = [];
    const newMessageCount = useSelector(state => state.chatInput.newMessageCount);
    const [showDivider, setShowDivider] = useState(-1);
    const shouldAutoScroll = useSelector(state => state.chatInput.shouldAutoScroll);
    const dispatch = useDispatch();
    const bottomReachedRef = useBottomScrollListener(() => dispatch({type: actions.TOGGLE_AUTO_SCROLL, shouldAutoScroll: true}), 0, 0);
    const scrollRef = useRef(null);
    const bottomRef = useRef(null);
    useEffect(() => {
        let index = props.messages.findIndex(msg => msg.readBy == null || !msg.readBy.includes(sessionStorage.getItem('userId')));
        if(index > 0)
        {
            setShowDivider(index);
            dispatch({type: actions.TOGGLE_AUTO_SCROLL, shouldAutoScroll: false});
        }
        else
        {
            setShowDivider(-1);
        }

    }, [dispatch, props.messages]);

    useEffect(() => {
        if(scrollRef != null && scrollRef.current != null)
            scrollRef.current.scrollIntoView({block: 'center'});
    }, []);

    useEffect(() => {
        if(shouldAutoScroll)
            bottomRef.current.scrollIntoView({block: 'end'});
    }, [shouldAutoScroll, props.messages]);

    chatEntries.push(props.messages.map((message, index) => {
        let chatEntry;
        if(index === showDivider)
        {
            chatEntry = <div key={1} ref={scrollRef} >{showDivider === -1 ? null : <UnReadDivider/>}</div>;
        }
        if(message.messageType === 'CARDS')
            return <React.Fragment key={message.messageId}>{chatEntry}<ChatCards messageId={message.messageId} chatCards={message.chipCards} onCardClick={props.onCardClick} heading={message.heading}/></React.Fragment>;
        else if(message.messageType === 'INFO')
            return <React.Fragment key={message.messageId}>{chatEntry}<ChatInfo message={message.message} colorType={message.colorType}/></React.Fragment>;
        else if(message.messageType === 'CHIPS')
            return <React.Fragment key={message.messageId}>{chatEntry}<ChatChips messageId={message.messageId} onClickChip={props.onChipClick} chips={message.chipCards} heading={message.heading}/></React.Fragment>;
        else if(message.messageType === 'IMP_EVENT')
            return <React.Fragment key={message.messageId}>{chatEntry}<ChatList impEvent={message.impEvent} heading={message.heading}/></React.Fragment>;
        else if(message.messageType === 'INFO_CARD')
            return <React.Fragment key={message.messageId}>{chatEntry}<InfoCard label={message.chipCards[0].label} heading={message.heading}/></React.Fragment>;
        else
            return <React.Fragment key={message.messageId}>{chatEntry}<ChatBubble  message={message.message} right={message.sender === props.sender}
                senderLabel={message.sender} time={message.time} className={message.className}/></React.Fragment>;
    }));

    return <><div className={classes.chatPane} onScroll={() => dispatch({type: actions.TOGGLE_AUTO_SCROLL, shouldAutoScroll: false})} ref={bottomReachedRef}>
        {chatEntries}
        {!shouldAutoScroll && <div className={classes.downArrow}>
            <IconButton size='small' className={classes.downArrowIcon} onClick={() => dispatch({type: actions.TOGGLE_AUTO_SCROLL, shouldAutoScroll: true})}>
            <Badge badgeContent={newMessageCount} anchorOrigin={{ horizontal: 'right', vertical: 'bottom'}} color='primary'>
                <ArrowDownward/>
            </Badge>
            </IconButton>
        </div>}
        <div ref={bottomRef}/>
        </div>

        <NoVoterToast/>
        </>
};

ChatPane.propTypes = {
    messages: PropTypes.array,
    onCardClick: PropTypes.func,
    onChipClick: PropTypes.func,
    sender: PropTypes.string,
};
