import React from 'react';
import {Grid, Chip, Badge} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {ChatEvent} from "./chat-event";

const useStyles = makeStyles(theme => ({
    chip: {
        margin: theme.spacing(1),
        marginBottom: theme.spacing(0),
        cursor: 'pointer',
    },
}));
export const ChatChips = props => {

    const classes = useStyles();
    const heading = props.heading.includes('Please cast your vote') &&
    props.chips.filter(chip => chip.selected != null && chip.selected.includes(sessionStorage.getItem('userId'))).length > 0 ? 'Thanks for Voting' : props.heading;
    const disabled = props.chips.filter(chip => chip.selected != null && chip.selected.includes(sessionStorage.getItem('userId'))).length > 0;
    const chips = props.chips.map(chip => <Badge key={chip.label} variant={props.heading.includes('Please cast your vote') ? 'standard' : 'dot'}
                                                 badgeContent={chip.selected == null ? 0 : chip.selected.length}
                                                 color={chip.label === 'YES' ? 'primary' : 'secondary'}
                                                 overlap='circle'
                                                invisible={!disabled || chip.selected == null || chip.selected.length === 0}>
        <Chip onClick={() => props.onClickChip({...chip, messageId: props.messageId})}
        variant='outlined' className={classes.chip} disabled={disabled}
         label={chip.label}/>
        </Badge>);
    return <ChatEvent heading={heading}>
        <Grid container spacing={1} justify='center' alignItems='flex-end' alignContent='space-between'>
            <Grid item>
                {chips}
            </Grid>
        </Grid>
    </ChatEvent>;
};
ChatChips.propTypes = {
    chips: PropTypes.array,
    onClickChip: PropTypes.func,
    messageId: PropTypes.number,
    heading: PropTypes.string,
};
