import React from 'react';
import {
    Dialog, Grid, DialogContent, DialogTitle, Divider,
    Typography, IconButton, makeStyles
} from '@material-ui/core';
import CloseIcon  from '@material-ui/icons/Close';
import { emojiCodePoint } from './emoji-data';
import {useDispatch, useSelector} from "react-redux";
import { actions } from "../../actions/actions";

const useStyles = makeStyles(theme => ({
    content: {
        paddingLeft: theme.spacing(0),
    },
    gridItem: {
        marginBottom: theme.spacing(1),
        paddingLeft: theme.spacing(2),
        cursor: 'pointer',
    },
    emoji: {
        transition: 'transform .2s',
        display: 'inline-block',
        '&:hover': {
            transform: 'scale(1.3)',
        },
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
}));

const EmojiPicker = () => {
    const classes = useStyles();
    const isEmojiOpen = useSelector(state => state.chatInput.isEmojiOpen);
    const dispatch = useDispatch();

    const emojiList = emojiCodePoint.map(em => <Grid key={em} item xs={2} className={classes.gridItem}>
        <div className={classes.emoji} onClick={() => dispatch({type: actions.EMOJI_CLICK_ACTION, payload: em})} role='img' aria-label='grin-face'>{em}</div>
    </Grid>);

    return <Dialog open={isEmojiOpen} onBackdropClick={() => dispatch({type: actions.EMOJI_TOGGLE_ACTION})}>
        <DialogTitle disableTypography>
            <Typography variant="h6">Emoji Picker</Typography>
            <IconButton aria-label="close" className={classes.closeButton} onClick={() => dispatch({type: actions.EMOJI_TOGGLE_ACTION})}>
                <CloseIcon />
            </IconButton>
        </DialogTitle>
        <Divider variant='middle'/>
        <DialogContent className={classes.content}>
            <Grid container>
                {emojiList}
            </Grid>
        </DialogContent>
    </Dialog>;
};

export default EmojiPicker;
