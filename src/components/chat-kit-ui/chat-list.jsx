import React from 'react';
import {
    Grid,
    ListItem, ListItemText
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {ChatEvent} from "./chat-event";
import {ChatCard} from "./chat-card";
import {getRoleImage, GetRoleColorClass} from "../../role-fetcher";


const useStyles = makeStyles(theme => ({
    listItem: {
        padding: theme.spacing(0),
        margin: theme.spacing(0),
    }
}));

export const ChatList = props =>
{
    const classes = useStyles();

    const roles = props.impEvent.players == null ? null : props.impEvent.players.map(p => <ListItem key={p.name} className={classes.listItem}>
        <ListItemText primary={p.name} secondary={p.role}>
        </ListItemText>
    </ListItem>);

    const policy = props.impEvent.policy == null ? null : <Grid container justify={'center'}><ChatCard role={props.impEvent.policy}
                                                                    image={getRoleImage(props.impEvent.policy)}
                                                                    color={GetRoleColorClass(props.impEvent.policy)}
    /></Grid>;

    return <ChatEvent heading={props.heading}>{roles} {policy}</ChatEvent>
};
ChatList.propTypes = {
    impEvent: PropTypes.object,
    heading: PropTypes.string,
};
