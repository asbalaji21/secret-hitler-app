import React, {useState} from "react";
import ChatHeader from "../chat-kit-ui/chat-header";
import {useDispatch, useSelector} from "react-redux";
import {actions} from "../../actions/actions";
import DrawerChildren from "../side-drawer-children";

export default () => {

    const players = useSelector(state => state.currentGame.players).map(p => p.name);
    const role = useSelector(state => state.currentGame.role);
    const typing = useSelector(state => state.chatInput.typing);
    const gameId = sessionStorage.getItem('gameId');
    const [drawerOpen, toggleDrawer] = useState(false);
    const dispatch = useDispatch();
    const startGameHandler = () => {
        toggleDrawer(false);
        dispatch({type: actions.START_GAME_ACTION});
    };

    const subTitle = typing == null ? null : `${typing} is typing...`;

    return  <ChatHeader drawerOpen={drawerOpen} toggleDrawer={() => toggleDrawer(!drawerOpen)}
                        drawerChildren={<DrawerChildren startGameHandler={startGameHandler}/>} title={`Game - ${gameId}`}
                        subTitle={subTitle} role={role} electPresident={() => {
                            dispatch({type: actions.ELECT_PRESIDENT_CLICK_ACTION});
                            dispatch({type: actions.ELECT_PRESIDENT_ACTION});
    }}/>
}
