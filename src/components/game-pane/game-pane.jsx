import React, {useEffect} from "react";
import GamePaneHeader from './game-pane-header';
import {ChatInput} from "../chat-kit-ui/chat-input";
import {useDispatch, useSelector} from "react-redux";
import {ChatPane} from "../chat-kit-ui/chat-pane";
import {actions} from "../../actions/actions";
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import EmojiPicker from "../chat-kit-ui/emoji-dialog";
import {getTriggerAction} from "../trigger-json";
import {gameEvent} from '../game-event';
import {BASE_URL} from '../../constant';


export const GamePane = () => {
    const messages = useSelector(state => state.messageReducer);
    const userId = sessionStorage.getItem('userId');
    const gameId = parseInt(sessionStorage.getItem('gameId'));
    const dispatch = useDispatch();
    useEffect(() => {
        const stompClient = Stomp.over(new SockJS(`${BASE_URL}/ws`));
        let subscription;
        stompClient.connect({gameId: gameId, userName: userId}, () => {
            subscription = stompClient.subscribe('/user/topic/onGameEvent',payload => gameEvent(payload, dispatch));
        }, error => alert(error));
        dispatch({type: actions.CURRENT_GAME_FETCH_ACTION, gameId: gameId, userId: userId});
        return () => {
            cleanUp(subscription, stompClient);
        }
    }, [dispatch, gameId, userId]);


    const cleanUp = (subscription, stompClient) => {
        try {
            if(subscription != null)
                subscription.unsubscribe();
            stompClient.disconnect();
        }
        catch (e) {
            //
        }
    };

    return <>
        <GamePaneHeader/>
        <ChatPane messages={messages} sender={userId} onChipClick={chip => {
            dispatch({type: actions.TRIGGER_ACTION, payload: getTriggerAction(chip)})
        }}
                  onCardClick={card => {
                      if((card.selected != null && card.selected.includes(sessionStorage.getItem('userId'))) || card.disabled)
                          return;
                      dispatch({type: actions.TRIGGER_ACTION, payload: getTriggerAction(card)})
                  }}/>
        <ChatInput/>
        <EmojiPicker/>
    </>
};


