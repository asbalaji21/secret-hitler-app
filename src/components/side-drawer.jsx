import React from 'react';
import {
    Drawer, List,
    makeStyles, ListSubheader
} from "@material-ui/core";
import PropTypes from "prop-types";

const useStyles = makeStyles({
    list: {
        width: 260,
    },
});

const SideDrawer = props => {
    const classes = useStyles();
    return <Drawer anchor='right' open={props.open} onClose={props.toggleDrawer}>
        <div className={classes.list} role='presentation'>
            <List subheader={<ListSubheader component='div'>Hello {sessionStorage.getItem('userId')}</ListSubheader>}>
                {props.drawerChildren}
            </List>
        </div>
    </Drawer>
};

SideDrawer.propTypes =
{
    toggleDrawer: PropTypes.func,
    drawerChildren: PropTypes.object,
};

export default SideDrawer;
