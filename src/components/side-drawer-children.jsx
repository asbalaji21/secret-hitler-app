import React from 'react';
import {
    Avatar, createMuiTheme, ListItem,
    ListItemAvatar, ListItemIcon, ListItemText, Divider, Badge, ListItemSecondaryAction, IconButton
} from "@material-ui/core";
import {Games} from "@material-ui/icons";
import {useSelector} from "react-redux";
import PropTypes from 'prop-types';
import {getRoleImage} from '../role-fetcher';
import { getSpecialPowerInfo } from "./special-power-info";
import vetoIcon from '../assets/veto_icon.png';

const styles = {
    offline: {
    height: '15px',
    width: '15px',
    backgroundColor: '#bbb',
    borderRadius: '50%',
    display: 'inline-block',
    marginRight: '15px',
    },
    online: {
        height: '15px',
        width: '15px',
        backgroundColor: 'green',
        borderRadius: '50%',
        display: 'inline-block',
        marginRight: '15px',
    }
};

const SideDrawerChildren = props => {
    const theme = createMuiTheme();
    const players = useSelector(state => state.currentGame.players);
    const totalPlayers = useSelector(state => state.currentGame.totalPlayers);
    const gameStatus = useSelector(state => state.currentGame.gameStatus);
    const startGameText = totalPlayers !== players.length ? `- ${players.length}/${totalPlayers} joined` : '';
    const gamePlay = useSelector(state => state.gamePlay);
    const currentGame = useSelector(state => state.currentGame);
    const teamMapping = currentGame.players.map(p => <ListItem key={p.name}>
        <span style={p.offline ? styles.offline : styles.online}/>
        <ListItemText primary={`${p.name}`}/>
        {currentGame.role != null && (currentGame.role === 'FASCIST' || (totalPlayers === 5 && currentGame.role !== 'LIBERAL')) && <ListItemSecondaryAction>
            <IconButton edge='end'>
                <img style={{ width: '15px', height: '15px' }} src={getRoleImage(p.role)} alt='role'/>
            </IconButton>
        </ListItemSecondaryAction>}
    </ListItem>);

    return [
        <React.Fragment key={1}><ListItem button onClick={props.startGameHandler} disabled={totalPlayers !== players.length || gameStatus === 'STARTED'}>
            <ListItemIcon>
                <Games/>
            </ListItemIcon>
            <ListItemText primary={`Start Game ${startGameText}`}/>
        </ListItem><Divider/></React.Fragment>,

        <React.Fragment key={2}><ListItem>

            <ListItemAvatar>
                <Badge badgeContent={<Avatar src={vetoIcon} style={{ width: 18, height: 18}}/>} invisible={gamePlay.fascistPolicy !== 5} anchorOrigin={{ horizontal:'right', vertical: 'bottom'}} overlap='circle'>
                <Avatar style={{backgroundColor: theme.palette.primary.main}}>{gamePlay.liberalPolicy}</Avatar>
                </Badge>
                </ListItemAvatar>

            <ListItemText primary='Liberal policies' secondary={`${gamePlay.liberalPolicy}/5 policies enacted`}/>
        </ListItem>
        <ListItem><ListItemText inset secondary={` - ${gamePlay.electionTracker}/3 elections failed`}/></ListItem>
            <Divider/>
        </React.Fragment>,

        <React.Fragment key={3}><ListItem>
            <ListItemAvatar>
                <Badge badgeContent={<Avatar src={getRoleImage('HITLER')} style={{ width: 24, height: 24}}/>} anchorOrigin={{ horizontal:'right', vertical: 'bottom'}}
                       overlap='circle' invisible={gamePlay.fascistPolicy < 3}>
                <Avatar style={{backgroundColor: theme.palette.secondary.main}}>{gamePlay.fascistPolicy}</Avatar>
                </Badge>
            </ListItemAvatar>
            <ListItemText primary='Fascist policies' secondary={`${gamePlay.fascistPolicy}/6 policies enacted`}/>
        </ListItem><ListItem><ListItemText inset secondary={` - ${getSpecialPowerInfo(totalPlayers, gamePlay.fascistPolicy)}`}/></ListItem><Divider/></React.Fragment>,
    ].concat(teamMapping)
};
SideDrawerChildren.propTypes = {
    startGameHandler: PropTypes.func,
};

export default SideDrawerChildren;
