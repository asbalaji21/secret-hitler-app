import {actions} from "../actions/actions";

export const gameEvent = (payload, dispatch) => {
    switch (JSON.parse(payload.body).gameEvent) {
        case 'PLAYER_JOINED': {
            dispatch({type: actions.PLAYERS_JOINED_ACTION, player: JSON.parse(payload.body).message.split(' ')[0]});
            dispatch({type: actions.CURRENT_MESSAGE_RECEIVED_ACTION, payload: JSON.parse(payload.body)});
            break;
        }
        case 'REVEAL_ROLE': {
            const temp = JSON.parse(payload.body);
            if(temp.target == null || temp.target.includes(sessionStorage.getItem('userId')))
            {
                dispatch({type: actions.CURRENT_MESSAGE_RECEIVED_ACTION, payload: temp});
                dispatch({type: actions.START_GAME_SUCCESS_ACTION, payload: {role: temp.chipCards[0].label}});
            }
            break;
        }
        case 'REVEAL_TEAM_INFO': {
            const temp = JSON.parse(payload.body);
            if(temp.target == null || temp.target.includes(sessionStorage.getItem('userId')))
            {
                dispatch({type: actions.CURRENT_MESSAGE_RECEIVED_ACTION, payload: temp});
                dispatch({type: actions.TEAM_INFO_REVEALED_ACTION, payload: {players: temp.impEvent.players}});
            }
            break;
        }
        case 'MESSAGE_SENT': {
            const temp = JSON.parse(payload.body);
            if(temp.target == null || temp.target.includes(sessionStorage.getItem('userId')))
                dispatch({type: actions.CURRENT_MESSAGE_RECEIVED_ACTION, payload: temp});
            break;
        }
        case 'UPDATE_MESSAGE': {
            dispatch({type: actions.UPDATE_MESSAGE, payload: JSON.parse(payload.body)});
            break;
        }
        case 'VOTE_FAILED': {
            dispatch({type: actions.VOTE_FAILED_ACTION, payload: {
                    electionTracker: parseInt(JSON.parse(payload.body).message[0]),
                }});
            dispatch({type: actions.CURRENT_MESSAGE_RECEIVED_ACTION, payload: JSON.parse(payload.body)});
            break;
        }
        case 'ELECTION_FAILED': {
            dispatch({type: actions.VOTE_FAILED_ACTION, payload: {
                    electionTracker: 0,
                }});
            dispatch({type: actions.CURRENT_MESSAGE_RECEIVED_ACTION, payload: JSON.parse(payload.body)});
            break;
        }
        case 'POLICY_ENACTED': {
            dispatch({type: actions.CURRENT_MESSAGE_RECEIVED_ACTION, payload: JSON.parse(payload.body)});
            dispatch({type: actions.POLICY_ENACTED_ACTION, payload: {liberal: JSON.parse(payload.body).chipCards[0].label === 'LIBERAL'}});
            break;
        }
        case 'ELECT_PRESIDENT': {
            dispatch({type: actions.ELECT_PRESIDENT, payload: {president: JSON.parse(payload.body).message}});
            break;
        }
        case 'ALL_VOTES_RECEIVED': {
            dispatch({type: actions.NO_VOTERS_ACTION, payload: {players: JSON.parse(payload.body).impEvent.players}});
            break;
        }
        case 'TYPING': {
            const temp = JSON.parse(payload.body);
            dispatch({type: actions.UPDATE_TYPING_ACTION, payload: {sender: temp.sender, typing: temp.notifyFlag}});
            break;
        }
        case 'ONLINE_OFFLINE': {
            const temp = JSON.parse(payload.body);
            dispatch({type: actions.ONLINE_OFFLINE_ACTION, payload: {sender: temp.sender, notifyFlag: temp.notifyFlag}});
            break;
        }
        case 'SPL_POWER_USED': {
            dispatch({type: actions.SPL_POWER_USED_ACTION});
            break;
        }
        default: break;
    }
};
