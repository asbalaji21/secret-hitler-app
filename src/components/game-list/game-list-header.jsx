import React, { useEffect } from 'react';
import {useDispatch, useSelector} from "react-redux";
import ChatHeader from "../chat-kit-ui/chat-header";
import {actions} from "../../actions/actions";

export default () => {
    const userId = sessionStorage.getItem('userId');
    const gameList = useSelector(state => state.fetchActiveGames);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch({type: actions.ACTIVE_GAMES_FETCH_ACTION, userId: userId})
    }, [dispatch, userId]);
    return <ChatHeader title={userId} subTitle={`${gameList.length} active game(s)`}/>
};
