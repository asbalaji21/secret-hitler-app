import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import PropTypes from 'prop-types';

const useStyles = makeStyles({
    root: {
        flexGrow: 1,
    },
});

const TabContent = props => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <Paper square className={classes.root}>
            <Tabs
                value={value}
                onChange={handleChange}
                variant="fullWidth"
                indicatorColor="primary"
                textColor="primary"
            >
                <Tab label={'Create Game'} />
                <Tab label={'Join Game'} />
            </Tabs>
            <TabPanel value={value} index={0}>
                {props.createContent}
            </TabPanel>
            <TabPanel value={value} index={1}>
                {props.joinContent}
            </TabPanel>
        </Paper>
    );
};

TabContent.propTypes = {
    createContent: PropTypes.any.isRequired,
    joinContent: PropTypes.any.isRequired,
};

function TabPanel(props) {
    const { children, value, index } = props;

    return (
        <Typography
            component="div"
            hidden={value !== index}
            id={`nav-tabPanel-${index}`}
        >
            <Box p={3}>{children}</Box>
        </Typography>
    );
}

TabPanel.propTypes = {
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

export default TabContent;
