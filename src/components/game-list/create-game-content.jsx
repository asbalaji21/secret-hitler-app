import React, {useEffect, useState} from 'react';
import {Typography, Slider, Button, makeStyles} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {actions} from "../../actions/actions";
import {useHistory} from "react-router";

const useStyles = makeStyles(theme => ({
    content: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
    },
}));

export default () => {
    const classes = useStyles();
    const [totalPlayers, setTotalPlayers] = useState(5);
    const gameId = useSelector(state => state.createJoinGame.gameId);
    const dispatch = useDispatch();
    const userId = sessionStorage.getItem('userId');
    const history = useHistory();
    const CreateGameHandler = () => {
            dispatch({type: actions.GAME_CREATE_ACTION, payload:{
                        userId: userId,
                        totalPlayers: totalPlayers
                    }});
    };

    useEffect(() => {
        if(gameId !== 0)
        {
            async function saveGameId(){
                await sessionStorage.setItem('gameId', gameId);
            }
            saveGameId().then(()=> history.push('/game'));
        }
    }, [gameId, history]);

    return <div className={classes.content}>
        <Typography gutterBottom>
            Total Players
        </Typography>
        <Slider
            style={{ width: '200px'}}
            defaultValue={5}
            step={1}
            valueLabelDisplay="auto"
            min={5}
            max={10}
            value={totalPlayers}
            onChange={(e, val) => setTotalPlayers(val)}
        />
        <Button style={{margin: '16px', float: 'right'}} variant="contained" onClick={CreateGameHandler}>Create
            Game</Button>
    </div>
};
