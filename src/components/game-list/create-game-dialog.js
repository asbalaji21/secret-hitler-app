import React from 'react';
import {
    Dialog,
    Slide
} from "@material-ui/core";
import TabContent from "./TabContent";
import {useDispatch, useSelector} from "react-redux";
import {actions} from "../../actions/actions";
import CreateGameContent from './create-game-content';
import JoinGameContent from './join-game-content';


export default () => {
    const open = useSelector(state => state.createJoinGame.isDialogOpen);
    const dispatch = useDispatch();
    return <Dialog open={open} TransitionComponent={Transition}
    onClose={() => dispatch({type: actions.GAME_DIALOG_TOGGLE_ACTION})}>
        <TabContent createContent={CreateGameContent} joinContent={JoinGameContent}/>
    </Dialog>
};



const Transition = React.forwardRef(function Transition(props, ref) {
return <Slide direction="up" ref={ref} {...props} />;
});
