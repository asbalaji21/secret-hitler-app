import React, {useEffect, useState} from 'react';
import {TextField, Button, makeStyles, CircularProgress} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {actions} from "../../actions/actions";
import {useHistory} from "react-router";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

const useStyles = makeStyles(theme => ({
    content: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
    },
    buttonProgress: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
}));

const isValidGameId = gameId => gameId != null && gameId !== '' && /^[0-9]*$/g.test(gameId);

export default () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [gameId, setGameId] = useState(0);
    const userId = sessionStorage.getItem('userId');
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);
    const history = useHistory();
    const selectedGameId = useSelector(state => state.createJoinGame.gameId);
    const [isBot, setBot] = useState(false);

    const validateAndSetGameId = gameId => {
        setGameId(gameId);
        setError(!isValidGameId(gameId));
    };

    const joinGameHandler = () => {
        setLoading(true);
        dispatch({type: actions.GAME_JOIN_ACTION, gameId: gameId, userId: userId, isBot: isBot})
    };

    useEffect(() => {
        if(selectedGameId === 0)return;
        async function saveGameId(){
            await sessionStorage.setItem('gameId', selectedGameId);
        }
        saveGameId().then(()=> history.push('/game'));
    }, [selectedGameId, history]);

    return <div className={classes.content}>
        <TextField type='number' onChange={event => validateAndSetGameId(event.target.value)} label='Game ID'
                   helperText={error ? 'Game Id is Invalid!!' : null} error={error} style={{ width: '50%'}}/>
            <FormControlLabel style={{ paddingLeft: '24px'}}
                control={
                    <Switch
                        checked={isBot}
                        onChange={() => setBot(!isBot)}
                        value="isBot"
                        color="primary"
                    />
                }
                label={isBot ? "Bot" : "Human"}
                labelPlacement={'Top'}
            />
        <Button style={{margin: '16px', float: 'right'}} variant="contained" onClick={joinGameHandler}
                disabled={error || gameId === 0 || loading}>Join Game
            {loading && <CircularProgress className={classes.buttonProgress} size={24}/>}
        </Button>
    </div>
}
