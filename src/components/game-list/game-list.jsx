import React from 'react';
import GameListHeader from './game-list-header';
import { List, ListItem, ListItemAvatar, Avatar, ListItemText, Typography, makeStyles,
    Divider, Fab } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import {useHistory} from "react-router";
import {getRoleColor, getRoleImage} from "../../role-fetcher";
import {useDispatch, useSelector} from "react-redux";
import {actions} from "../../actions/actions";
import CreateJoinDialog from './create-game-dialog';

const useStyles = makeStyles({
    inline: {
        display: 'inline',
    },
    listItem: {
        cursor: 'pointer'
    },
    fab: {
        margin: 0,
        top: 'auto',
        right: 20,
        bottom: 20,
        left: 'auto',
        position: 'fixed'
    },
    notificationDisplay: {
        display: 'inline',
        color: 'green',
        fontWeight: 'bold',
    }
});
export default () => {
    const classes = useStyles();
    const userId = sessionStorage.getItem('userId');
    const gameList = useSelector(state => state.fetchActiveGames);
    const history = useHistory();
    const dispatch = useDispatch();



    const openGame = async gameId => {
        await sessionStorage.setItem('gameId', gameId);
        history.push('/game')
    };

    const getSecondary = game =>
    {
        return game.players.filter(p => p.name === userId).map(p => {
            return p.hasUnSeenGameEvents ?  <Typography key={1}
                component="span"
                variant="caption"
                className={classes.notificationDisplay}
                color="textPrimary"
            >
                You have new Game Events...
            </Typography> : <React.Fragment key={2}>
                <Typography
                    component="span"
                    variant="caption"
                    className={classes.inline}
                    color="textPrimary"
                >
                    Players&nbsp;-&nbsp;
                </Typography>
                {game.players.map(p => p.name).join(', ')}
            </React.Fragment>
        })
    };

    const constructListItemText = game =>
        <ListItemText primary={`Game - ${game.gameId}`} secondary={
            getSecondary(game)
        }>
        </ListItemText>;

    const listItems = gameList.map(game => <React.Fragment key={game.gameId}><ListItem className={classes.listItem}
        onClick={() => openGame(game.gameId)}>
        <ListItemAvatar>
            <Avatar alt='list image' src={getRoleImage(game.players != null && game.players[userId] ? game.players[userId].role : null)}
            style={{backgroundColor: getRoleColor(game.players != null && game.players[userId]? game.players[userId].role : null)}}/>
        </ListItemAvatar>
            {constructListItemText(game)}
    </ListItem>
    <Divider variant='inset'/> </React.Fragment>
    );

    return <>
        <GameListHeader/>
            <List>
                {listItems}
            </List>
            <Fab className={classes.fab} color='primary' onClick={() => dispatch({type: actions.GAME_DIALOG_TOGGLE_ACTION})}>
                <AddIcon/>
            </Fab>
        <CreateJoinDialog/>
        </>
};

