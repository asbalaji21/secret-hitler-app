import React, { useState } from 'react';
import { Container, CssBaseline, makeStyles, Paper, Typography
    , Fab, TextField } from '@material-ui/core';
import { ArrowForward } from "@material-ui/icons";
import { useHistory } from "react-router";

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        marginRight: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    header: {
        padding: theme.spacing(2),
    },
    textField: {
        margin: theme.spacing(1),
        width: 275,
    },
    button: {
        margin: theme.spacing(1),
        marginBottom: theme.spacing(2),
        left: 130,
    },
    copyrights: {
        marginTop: theme.spacing(10),
    }
}));



const isValidNickName = userId => userId != null && userId !== '' && /^[0-9 A-z]*$/g.test(userId);

export default () => {
    const [userId, setUserId] = useState('');
    const [error, setError] = useState(false);
    const classes = useStyles();
    const history = useHistory();

    const saveNickName = async () => {
        await sessionStorage.setItem('userId', userId);
        history.push('/games');
    };

    const setUserName = nickName => {
        setUserId(nickName);
        setError(!isValidNickName(nickName));
    };

    return <Container component="main" maxWidth="xs">
        <CssBaseline/>
        <Paper  className={classes.paper}>
            <Typography variant='h6' className={classes.header}>
                Secret Hitler
            </Typography>
            <TextField className={classes.textField} onChange={event => setUserName(event.target.value)}
                       label="Nick Name" helperText={error ? 'Nick Name is Invalid!!' : null} error={error}
            />
            <Fab className={classes.button} size='small' aria-label='arrow-forward' color='primary'
                 onClick={saveNickName} disabled={error || userId === ''}>
                <ArrowForward/>
            </Fab>
        </Paper>
        <Typography className={classes.copyrights} gutterBottom>
            Secret Hitler was created by Mike Boxleiter,
            Tommy Maranges, Max Temkin, and Mac Schubert.
        </Typography>
        <Typography>
            Secret Hitler is licensed under a Creative
            Commons Attribution-NonCommercial-ShareAlike 4.0
            International License.
        </Typography>
    </Container>
};
