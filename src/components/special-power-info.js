export const getSpecialPowerInfo = (totalPlayers, fascistNo) => {
    switch (totalPlayers) {
        case 5:
        case 6: return getSmallPlayerInfo(fascistNo);
        case 7:
        case 8: return getMediumPlayerInfo(fascistNo);
        case 9:
        case 10: return getLargePlayerInfo(fascistNo);
        default: return '';
    }
};

const getSmallPlayerInfo = fascistNo => {
    switch (fascistNo) {
        case 0:
        case 1: return 'Peek Policy at 3';
        case 2: return 'Next: Peek Policy';
        case 3:
        case 4: return 'Next: Assassin';
        default: return '';
    }
};

const getMediumPlayerInfo = fascistNo => {
    switch (fascistNo) {
        case 0: return 'Investigate at 2';
        case 1: return 'Next: Investigation';
        case 2: return 'Next: Election';
        case 3:
        case 4: return 'Next: Assassin';
        default: return '';
    }
};

const getLargePlayerInfo = fascistNo => {
    switch (fascistNo) {
        case 0:
        case 1: return 'Next: Investigation';
        case 2: return 'Next: Election';
        case 3:
        case 4: return 'Next: Assassin';
        default: return '';
    }
};
