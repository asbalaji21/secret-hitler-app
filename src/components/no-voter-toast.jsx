import React from 'react';
import {Snackbar} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {actions} from "../actions/actions";

export const NoVoterToast = () => {
    const shouldShowToast = useSelector(state => state.noVotersReducer.shouldShowToast);
    const toastMessage = useSelector(state => state.noVotersReducer.message);
    const dispatch = useDispatch();
    return <Snackbar
        open={shouldShowToast}
        onClose={() => dispatch({type: actions.TOAST_HIDE_ACTION})}
        autoHideDuration={2000}
        message={<span>{toastMessage}</span>}
    />
};

