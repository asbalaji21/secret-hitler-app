export const getTriggerAction = data => {
    return {
        gameId: sessionStorage.getItem('gameId'),
        messageId: data.messageId,
        chipCardId: data.chipCardId,
        sender: sessionStorage.getItem('userId'),
        action: data.action,
    }
};


