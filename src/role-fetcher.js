import { makeStyles } from '@material-ui/core';
import liberal from './assets/liberal.png';
import fascist from './assets/facist.png';
import hitler from './assets/hitler.png';
import veto from './assets/veto_icon.png';

import {createMuiTheme} from "@material-ui/core";

const useCommonStyles = makeStyles(theme => ({

    liberal: {
        backgroundColor: theme.palette.primary.light,
    },
    fascist: {
        backgroundColor: theme.palette.secondary.light,
    },
    neutral: {
        backgroundColor: '#075E54',
    },
    defaultInfo: {
      backgroundColor: '#FFF5C4',
    },
    specificInfo: {
        backgroundColor: '#D4EAF4',
    },
    veto: {
        backgroundColor: '#FFF',
    },
}));

const theme = createMuiTheme();

export const getRoleImage = role => {
    switch (role) {
        case 'LIBERAL': return liberal;
        case 'HITLER': return hitler;
        case 'FASCIST': return fascist;
        case 'VETO': return veto;
        default: return null;
    }
};

export const GetRoleColorClass = role => {
    const commonClasses = useCommonStyles();
    switch (role) {
        case 'LIBERAL': return commonClasses.liberal;
        case 'HITLER': return commonClasses.fascist;
        case 'FASCIST': return commonClasses.fascist;
        case 'NEUTRAL': return commonClasses.neutral;
        case 'SPECIFIC': return commonClasses.specificInfo;
        case 'VETO': return commonClasses.veto;
        default: return commonClasses.defaultInfo;
    }
};

export const getRoleColor = role => {
    switch (role) {
        case 'LIBERAL': return theme.palette.primary.light;
        case 'HITLER': return theme.palette.secondary.light;
        case 'FASCIST': return theme.palette.secondary.light;
        default: return 'grey';
    }
};
