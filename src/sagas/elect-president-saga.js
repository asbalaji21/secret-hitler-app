import { axiosInstance } from '../axios-instance';

export default function* electPresidentSaga(action) {
    yield axiosInstance.put(`game/startNextRound/game/${sessionStorage.getItem('gameId')}`);
}
