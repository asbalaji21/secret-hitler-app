import { axiosInstance } from '../axios-instance';

export default function* startGameSaga() {
    yield axiosInstance.put(`game/start/user/${sessionStorage.getItem('userId')}/game/${sessionStorage.getItem('gameId')}`);
}
