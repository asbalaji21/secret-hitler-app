import { takeLatest, takeEvery } from 'redux-saga/effects';
import { actions } from "../actions/actions";
import sendMessageSaga, {onlineOfflineUpdateSaga, triggerActionSaga, typingSaga} from "./send-message-saga";
import fetchActiveGamesSaga from "./fetch-active-games-saga";
import {createGameSaga, joinGameSaga} from "./create-game-saga";
import fetchCurrentGameSaga from "./fetch-current-game-saga";
import startGameSaga from "./start-game-saga";
import electPresidentSaga from './elect-president-saga';

export default function* saga() {
    yield takeLatest(actions.MESSAGE_SEND_ACTION, sendMessageSaga);
    yield takeLatest(actions.ACTIVE_GAMES_FETCH_ACTION, fetchActiveGamesSaga);
    yield takeLatest(actions.GAME_CREATE_ACTION, createGameSaga);
    yield takeLatest(actions.GAME_JOIN_ACTION, joinGameSaga);
    yield takeLatest(actions.CURRENT_GAME_FETCH_ACTION, fetchCurrentGameSaga);
    yield takeLatest(actions.START_GAME_ACTION, startGameSaga);
    yield takeEvery(actions.TRIGGER_ACTION, triggerActionSaga);
    yield takeEvery(actions.ELECT_PRESIDENT_ACTION, electPresidentSaga);
    yield takeLatest(actions.TYPING_ACTION, typingSaga);
    yield takeLatest(actions.ONLINE_OFFLINE_STATUS_UPDATE_ACTION, onlineOfflineUpdateSaga);
}
