import { put } from 'redux-saga/effects';
import { axiosInstance } from '../axios-instance';
import {actions} from "../actions/actions";

export default function* fetchActiveGamesSaga(action) {
    const response = yield axiosInstance.get(`game/allGames/user/${action.userId}`);
    yield put({type: actions.ACTIVE_GAMES_FETCHED_SUCCESS_ACTION, payload: response.data});
}
