import { put } from 'redux-saga/effects';
import { axiosInstance } from '../axios-instance';
import {actions} from "../actions/actions";

export default function* fetchCurrentGameSaga(action) {
    if(action.gameId !== 0)
    {
        const response = yield axiosInstance.get(`game/getGameInfo/user/${action.userId}/game/${action.gameId}`);
        yield put({type: actions.CURRENT_GAME_FETCHED_SUCCESS_ACTION, payload: response.data, userId: action.userId});
        yield put({type: actions.ALL_MESSAGES_RECEIVED_ACTION, payload: response.data.messageContents});
        yield put ({type: actions.GAME_PLAY_FETCH_ACTION, payload:
                {
                    policy: response.data.policyHolder,
                    electionTracker: response.data.assembly == null ? 0 : response.data.assembly.electionTracker,
                    president: response.data.assembly == null ? null : response.data.assembly.president,
                    roundCompleted: response.data.assembly == null ? null : response.data.assembly.roundCompleted
                }});
    }
}

