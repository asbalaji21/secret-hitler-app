import stompClient from '../stomp-instance';
import {axiosInstance} from "../axios-instance";


export default function* sendMessageSaga(action) {
    yield stompClient.send('/api/sendMessage', {}, JSON.stringify({gameId: action.payload.gameId, message: action.payload.message, sender: action.payload.sender}));
}

export function* triggerActionSaga(action) {
    yield axiosInstance.put('game/triggerAction', action.payload);
}

export function* typingSaga(action) {
    yield stompClient.send('/api/typing', {}, JSON.stringify({gameId: action.payload.gameId, sender: action.payload.sender, isNotifyFlag: action.payload.isNotifyFlag}));
}

export function* onlineOfflineUpdateSaga(action) {
    yield stompClient.send('/api/sendOnlineStatus', {}, JSON.stringify({gameId: action.payload.gameId, sender: action.payload.sender, isNotifyFlag: action.payload.isNotifyFlag}));
}
