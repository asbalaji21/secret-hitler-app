import {axiosInstance} from "../axios-instance";
import {actions} from "../actions/actions";
import { put } from 'redux-saga/effects';

export function* createGameSaga(action)
{
    const response = yield axiosInstance.post('game/create', {totalPlayers: action.payload.totalPlayers, createdBy: action.payload.userId});
    yield put({type: actions.GAME_JOIN_ACTION, gameId: response.data.gameId, userId: action.payload.userId, isBot: false})
}

export function* joinGameSaga(action)
{
    yield axiosInstance.get(`game/join/user/${action.userId}/game/${action.gameId}/${action.isBot}`);
    yield put({type: actions.GAME_JOINED_ACTION, gameId: action.gameId})
}
