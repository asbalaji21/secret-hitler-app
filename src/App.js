import React from 'react';
import LoginPage from "./components/login-page";
import {Route, Switch} from "react-router";
import {BrowserRouter} from "react-router-dom";
import GameList from './components/game-list/game-list';
import {GamePane} from "./components/game-pane/game-pane";



function App() {
      return (
              <BrowserRouter>
                  <Switch>
                      <Route exact path='/' component={LoginPage}/>
                      <Route exact path='/games' component={GameList}/>
                      <Route exact path='/game' component={GamePane}/>
                  </Switch>
              </BrowserRouter>
      );
}

export default App;
