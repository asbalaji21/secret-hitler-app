import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import {BASE_URL} from './constant';

const SockJSR = new SockJS(`${BASE_URL}/ws`);
export default Stomp.over(SockJSR);
